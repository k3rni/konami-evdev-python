import evdev
from evdev import InputDevice
from evdev import ecodes

def reject_device(caps):
    if ecodes.EV_KEY not in caps:
        # Possibly a switch: lid, microphone, dock etc.
        return "switch"
    if ecodes.EV_ABS not in caps and ecodes.EV_REL not in caps:
        # Likely a keyboard
        return "keyboard"
    if ecodes.EV_REL in caps and ecodes.REL_WHEEL in caps[ecodes.EV_REL]:
        # Has wheels, so probably a mouse
        return "mouse"

    return False

def button_pairs():
    return (
        {ecodes.BTN_A, ecodes.BTN_B}, # Most gamepads
        {ecodes.BTN_THUMB, ecodes.BTN_THUMB2} # Logitech RumblePad 2
    )

def check_abs_axes(caps):
    if ecodes.EV_ABS in caps:
        abs_axes = [ax for (ax, absinfo) in caps[ecodes.EV_ABS]]
        return {ecodes.ABS_X, ecodes.ABS_Y}.issubset(abs_axes)

    return False

# Unused, we don't care about rel axes
def check_rel_axes(caps):
    if ecodes.EV_REL in caps:
        rel_axes = caps[ecodes.EV_REL]
        return {ecodes.REL_X, ecodes.REL_Y}.issubset(rel_axes)

    return False

def detect_gamepad(path):
    dev = InputDevice(path)
    caps = dev.capabilities()

    if reject_device(caps):
        return False

    buttons = caps[ecodes.EV_KEY]
    valid_buttons = any(pair.issubset(buttons) for pair in button_pairs())

    valid_abs_axes = check_abs_axes(caps)

    return valid_abs_axes and valid_buttons
