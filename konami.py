from evdev import ecodes, events
from evdev.util import categorize

def rich(ev):
    sec, usec = ev.sec, ev.usec
    evt = ecodes.EV[ev.type][3:] # trim EV_
    lookup = getattr(ecodes, evt)
    try:
        ec = lookup[ev.code]
    except KeyError: # when EV_KEY but what's returned is a button
        evt = 'BTN'
        ec = ecodes.BTN[ev.code]

    return "{}.{} {}({}, {})".format(sec, usec, evt, ec, ev.value)

def diffs(a):
    if len(a) < 2: return []
    return [a[k] - a[k-1] for k in range(1, len(a))]

UP, DOWN, LEFT, RIGHT, AA, BB = range(6) # cheap enum-like

class KonamiRecognizer(object):
    CODE = (UP, UP, DOWN, DOWN, LEFT, RIGHT, LEFT, RIGHT, BB, AA, -1)

    def __init__(self, absinfo):
        self.progress = 0
        self.absinfo = absinfo
        self.xprev = None
        self.yprev = None
        self.yaxes = {ecodes.ABS_Y, ecodes.ABS_RY, ecodes.ABS_HAT0Y}
        self.xaxes = {ecodes.ABS_X, ecodes.ABS_RX, ecodes.ABS_HAT0X}
        self.abtns = {ecodes.BTN_A, ecodes.BTN_THUMB}
        self.bbtns = {ecodes.BTN_B, ecodes.BTN_THUMB2}

    def emit(self, dir_or_btn):
        if KonamiRecognizer.CODE[self.progress] == dir_or_btn:
            self.progress += 1
        else:
            self.progress = 0

    def success(self):
        return KonamiRecognizer.CODE[self.progress] < 0

    def process(self, event):
        ev = categorize(event)
        if isinstance(ev, events.KeyEvent):
            self.process_key(ev)
        elif isinstance(ev, events.AbsEvent):
            self.process_abs(ev)

    def process_key(self, keyev):
        ev = keyev.event
        code, v = ev.code, ev.value
        if v != 0: return # we care only about button released
        if code in self.abtns:
            self.emit(AA)
        if code in self.bbtns:
            self.emit(BB)

    def process_abs(self, absev):
        ev = absev.event
        code, v = ev.code, ev.value
        # print(rich(ev))
        lt, ht = self.axis_zones(code)

        if code in self.yaxes:
            if self.yprev is None: self.yprev = self.axis_center(code)
            # A Schmitt trigger: two thresholds, emit state only once crossed
            if v >= ht:
                if self.yprev < ht: self.emit(DOWN)
            elif v <= lt:
                if self.yprev > lt: self.emit(UP)
            self.yprev = v
        if code in self.xaxes:
            if self.xprev is None: self.xprev = self.axis_center(code)
            if v >= ht:
                if self.xprev < ht: self.emit(RIGHT)
            elif v <= lt:
                if self.xprev > lt: self.emit(LEFT)
            self.xprev = v

    def axis_zones(self, code):
        axinfo = self.absinfo[code]
        span = axinfo.max - axinfo.min
        return (axinfo.min + span / 4, axinfo.max - span / 4)

    def axis_center(self, code):
        axinfo = self.absinfo[code]
        return (axinfo.min + axinfo.max) / 2.0

