#! /usr/bin/env python

import asyncio
from asyncio import wait_for, TimeoutError
import evdev
from evdev import InputDevice, ecodes
from gamepad import detect_gamepad
from konami import KonamiRecognizer

async def expect_konami(path, timeout=None):
    dev = InputDevice(path)
    absinfo = dict(dev.capabilities()[ecodes.EV_ABS])
    konami = KonamiRecognizer(absinfo)

    loop = asyncio.get_running_loop()
    end_at = loop.time() + timeout

    while True:
        remaining = end_at - loop.time()
        try:
            event = await wait_for(dev.async_read_one(), remaining)
            konami.process(event)
            if konami.success():
                print("SUCCESS")
                break
        except TimeoutError:
            print("Time limit {}s expired".format(timeout))
            break

    dev.close() # Need to close explicitly before eventloop is terminated

def main():
    usable_devices = [path for path in evdev.list_devices() if detect_gamepad(path)]

    # Use the last one, because it was probably plugged in most recently
    path = sorted(usable_devices)[-1]
    print("Using {} ({})".format(path, InputDevice(path).name))

    asyncio.run(expect_konami(path, timeout=8))

if __name__ == "__main__":
    main()
